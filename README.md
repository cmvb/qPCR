# qPCR analysis

This repository contains a library using exported DRn & Ct files from the AB 7500 system, no other systems have been tested thus far.

## Getting Started

you can clone this git by starting a new project in R and choosing 'existing repository'

### Prerequisites

Several R packages need to be installed in order to run this software

```
install.packages("data.table") # to extract data from the csv files
install.packages("ggplot2") # visualisation library
install.packages("knitr") # constructing the latex pdf
install.packages("xtable") # making latex tables
```

### Example

An example can be found in the example folder, it is compiled with knitr and shows the current possible graphs.


<!---
## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 
--->
## Authors

* **Lorenz Feyen** - *Initial work* - [LorenzF](https://gitlab.com/LorenzF)

<!--See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.-->

## License

This project is licensed under the GNU License - see the [LICENSE](LICENSE) file for details

<!--
## Acknowledgments

* Acknowledgments go to my colleagues
-->
