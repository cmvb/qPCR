\documentclass[a4paper]{scrartcl}
%\usepackage[landscape]{geometry}

\usepackage[cm]{fullpage} % extending the page margins
% \usepackage{amsmath, amsfonts, url}
% \usepackage{graphicx}
\usepackage{pgf,tikz} % for displaying graphs in chunks
% \usetikzlibrary{arrows}
% \usepackage{rotating}
% \usepackage{verbatim}
% \usepackage{natbib}
% \usepackage[english]{babel}
% \usepackage{booktabs}
\usepackage{colortbl, xcolor}
% \usepackage{color,soul}
\usepackage{float} % necessary for positioning of chunks

\title{qPCR analysis example}
\author{Lorenz Feyen}
\date{\vspace{-3mm}}



% ==================
% = Start document =
% ==================
\begin{document}
%\SweaveOpts{concordance=TRUE}

<<setup, include=FALSE, cache=FALSE, echo=FALSE>>=
opts_chunk$set(echo=FALSE, dev = c('pdf'), fig.width=10, fig.height=6, fig.pos='H')
opts_knit$set(root.dir = "..")
#options(width = 90)
library(xtable)
library(knitr)
library(ggplot2)
library(data.table)
library(ggpubr)
setwd("..")
source("./qPCR.R") # load qPCR library


#qPCR <- readRDS("output.rds")

options(warn=-1) # we choose not to display warnings in the pdf
@

\maketitle

\section*{Introduction}

Here we will show some of the features the qPCR library has, first we need to load our data.

<<load_Induction, echo=TRUE>>=
list.files("./example")
file <- c("./example/2018_02_16-HPRT1-AVPR2+GAPDH+PECAM1_F8+TEK_VWF+CDH5_FCGR2B-HDMEC",
          "./example/2018_02_16-HPRT1-AVPR2+GAPDH+PECAM1_F8+TEK_VWF+CDH5_FCGR2B-HIMEC",
          "./example/2018_02_19-HPRT1-AVPR2+GAPDH+PECAM1_F8+TEK_VWF+CDH5-HCMEC",
          "./example/2018_02_20-HPRT1-AVPR2+GAPDH+PECAM1_F8+TEK_VWF+CDH5-HPMEC",
          "./example/2018_03_12-HPRT1-AVPR2+GAPDH+PECAM1_F8+TEK_VWF+CDH5-HUVEC")
          
qPCR <- getqPCRData(files = file, RNAdata = updateInfo(files = file, info = "./example/RNA.csv"),
                     readCt = FALSE, verbose = FALSE)
HDMEC <- list(`CTRL` = groupqPCRReplicates(name = "CTRL", replicates = c(qPCR$HDMEC_CTRL_A, qPCR$HDMEC_CTRL_B)),
               `LPS` = groupqPCRReplicates(name = "LPS", replicates = c(qPCR$HDMEC_LPS_A, qPCR$HDMEC_LPS_B)),
               `IL6` = groupqPCRReplicates(name = "IL6", replicates = c(qPCR$HDMEC_IL6_A, qPCR$HDMEC_IL6_B)),
               `PMA` = groupqPCRReplicates(name = "PMA", replicates = c(qPCR$HDMEC_PMA_A, qPCR$HDMEC_PMA_B)),
               `Histamin` = groupqPCRReplicates(name = "Histamin", replicates = c(qPCR$HDMEC_HIST_A, qPCR$HDMEC_HIST_B)),
               `Forskolin` = groupqPCRReplicates(name = "Forskolin", replicates = c(qPCR$HDMEC_FORSK_A, qPCR$HDMEC_FORSK_B)))
saveRDS(HDMEC, file = "./example/HDMEC.rds")
@

Let us have a look at the quality of the RNA inputted by the RNA.csv file, this needs to be done by the user itself.

<<RNA_Induction, echo=TRUE, results='asis'>>=
info <- getRNA(data = HDMEC)
rows <- seq(1, nrow(info), by = 2)
col <- rep("\\rowcolor[gray]{0.95}", length(rows))
print(xtable(info), booktabs = TRUE, 
  add.to.row = list(pos = as.list(rows), command = col), size="\\fontsize{7pt}{12pt}\\selectfont")
@

Then we go and calculate the Ct, DCt and DDCt based on the parameters.

<<calc_Induction, echo=TRUE>>=
HDMEC <- calcqPCRDDCt(data = calcqPCRDCt(data = calcqPCRCt(data = HDMEC), refgene = "HPRT1_JOE"), refsample = "CTRL")
@

\newpage
\section*{Ct values}

We can plot the Ct values in a heatmap

<<Ct_Induction, out.width='\\textwidth', fig.cap="", fig.show='hold', echo=TRUE>>=
HDMEC_Ct <- getCt(data = HDMEC)
HDMEC_Ct$heatmap
@

\newpage
\section*{DCt values}

We want to get relevant data out of the Ct values, so we will calculate the DCt

<<DCt_Induction, out.width='\\textwidth', fig.cap="", fig.show='hold', echo=TRUE>>=
HDMEC_DCt <- getDCt(data = HDMEC)
HDMEC_DCt$heatmap
@

While this gives us a nice overview of the data, we can not see everything because every gene has it's own range.

\newpage
\section*{DDCt values}

To get an even better view on the data we apply the DDCt comparison where we compare each gene to its Control counterpart.

<<DDCt_Induction, out.width='\\textwidth', fig.cap="", fig.show='hold', echo=TRUE>>=
HDMEC_DDCt <- getDDCt(data = HDMEC, refsample = "CTRL")
HDMEC_DDCt$heatmap
@

Can you see how all those near zero figures have turned into nice figures? On the contrary you have to be very careful because qPCR (especially with SYBR) is known to be able to generate signals from noise, therefore you always have to check your curves!

\newpage
\section*{Statistical Analysis}

ggplot2 (using ggpubr) also provides some nice functions to do statistical analysis, due to the complexity I did not build them into the functions.

<<Stat_Induction, out.width='.5\\textwidth', fig.cap="", fig.show='hold', echo=TRUE>>=
HDMEC_DDCt$boxplots$AVPR2 + stat_compare_means(comparisons = list(c("CTRL", "LPS"),c("CTRL", "Forskolin")), method = "wilcox.test", method.args = list(alternative = "two.sided"))
HDMEC_DDCt$boxplots$F8 + stat_compare_means(comparisons = list(c("CTRL", "LPS"),c("CTRL", "PMA")), method = "wilcox.test", method.args = list(alternative = "two.sided"))
HDMEC_DDCt$boxplots$vWF + stat_compare_means(comparisons = list(c("CTRL", "LPS"),c("CTRL", "PMA")), method = "wilcox.test", method.args = list(alternative = "two.sided"))
@

Not only gives this us more information on the distribution of the measurements but also we can perform practically any statistical test, I chose to go for a wilcoxon test since we have no idea if the data is normally distributed and we have a small pool of samples.
%we can see that for example AVPR2 of sample \textit{SVF\_VEC+} is $\Sexpr{round(SVF_DDCt$results[.("SVF_VEC+","AVPR2_FAM")]$DDCt,2)}$ times higher than the reference of \textit{SVF}

\newpage
\section*{Progression of gene expression}

When certain samples are collected during a protocol a progression in gene expression can be displayed. Let us collect the following data

<<load_LAT010, echo=TRUE>>=
file <- c("./example/2017_11_24-HPRT1-AVPR2+GAPDH+PECAM1_F8+TEK_VWF+CDH5_FCGR2B-HUVEC_LPS_IL6_LAT010")
          
LAT010 <- getqPCRData(files = file, RNAdata = updateInfo(files = file, info = "./example/RNA.csv"),
                     readCt = FALSE, verbose = FALSE)[c("LAT010_VAT","LAT010_UND", "LAT010_FLT", "LAT010_SVF", "LAT010_VEC-", "LAT010_VEC+", "LAT010_SVFCul", "LAT010_VECCul")]
LAT010 <- setqPCRnames(data = LAT010, names = c("Tissue","Undigested","Floating","Stromal","VECadherin-","VECadherin+","Stromal Cultured","VECadherin Cultured"))
saveRDS(LAT010, file = "./example/LAT010.rds")
@

Again calculate Ct, DCt and DDCt

<<calc_LAT010, echo=TRUE>>=
LAT010 <- calcqPCRDDCt(calcqPCRDCt(calcqPCRCt(data = LAT010), refgene = "HPRT1_JOE"), refsample = "LAT010_VAT")
@

now we can get some progression gene expression graphs using

<<DDCt_LAT010, out.width='.5\\textwidth', fig.cap="", fig.show='hold', echo=TRUE>>=
getDDCt(data = LAT010[c("LAT010_VAT","LAT010_SVF","LAT010_SVFCul")], genes = c("CDH5_CY5","PECAM1_CY5", "vWF_FAM"), refsample = "LAT010_VAT")$linegraph
getDDCt(data = LAT010[c("LAT010_VAT","LAT010_SVF","LAT010_VEC+","LAT010_VECCul")], genes = c("CDH5_CY5","PECAM1_CY5", "vWF_FAM"), refsample = "LAT010_VAT")$linegraph
getDDCt(data = LAT010[c("LAT010_VAT","LAT010_SVF","LAT010_SVFCul")], genes = c("AVPR2_FAM", "F8_FAM", "vWF_FAM"), refsample = "LAT010_VAT")$linegraph
getDDCt(data = LAT010[c("LAT010_VAT","LAT010_SVF","LAT010_VEC+","LAT010_VECCul")], genes = c("AVPR2_FAM", "F8_FAM", "vWF_FAM"), refsample = "LAT010_VAT")$linegraph
@

We see that the purification of cell out of the tissue is achieved, however when put into culture we seem to be losing expression

\newpage
\section*{Discussion}

This example should still be improved with more visualisation and better statistics

\end{document}